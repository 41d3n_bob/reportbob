#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "ltob.h"

int main(int argc, char* argv[])
{
	if(argc!=3)
	{
		printf("Usage : %s <file1> <file2>", argv[0]);
		exit(1);
	}

	FILE* f1 = fopen(argv[1], "rb");
	FILE* f2 = fopen(argv[2], "rb");

	uint32_t num1 = 0;
	uint32_t num2 = 0;

	fread(&num1, 1, 4, f1);
	fread(&num2, 1, 4, f2);

	ltob(&num1);
	ltob(&num2);

	uint32_t sum = num1+num2;

	printf("%d(%x) + %d(%x) = %d(%x)\n", num1, num1, num2, num2, sum, sum);
	
	return 0;
}


