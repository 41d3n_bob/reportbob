# Makefile
all: add-nbo

add-nbo: ltob.o main.o
	g++ -o add-nbo ltob.o main.o

main.o: ltob.h main.c

ltob.o: ltob.h ltob.c

clean:
	rm -f *.o